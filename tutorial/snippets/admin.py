from django.contrib import admin
from .models import Snippet


class SnippetAdmin(admin.ModelAdmin):
    ordering = ("-id",)
    list_display = ("id", "title", "code", "created")


# Register your models here.
admin.site.register(Snippet, SnippetAdmin)
